﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QWeb.Environments
{
    /// <summary>
    /// Local environment.
    /// </summary>
    public class Local
    {
        #region Properties

        /* 
         * App settings 
         */

        public static string SKIN_ID
        {
            get { return "100"; }
        }

        public static string SKIN_NAME
        {
            get { return "framework"; }
        }

        public static string DEFAULT_LANG
        {
            get { return "it"; }
        }

        /*
         * Front-end settings 
         */

        public static string HOMEURL
        {
            get { return "http://crm"; }
        }

        public static string HOMEBASE
        {
            get { return "/"; }
        }

        public static string[] WHITELIST
        {
            get { return new string[] { "self" }; }
        }

        /* 
         * API settings
         */

        public static string APIURL
        {
            get { return "http://yourapi.com"; }
        }

        public static string IMGURL
        {
            get { return "http://yourcdn.com"; }
        }

        /*
         * Cookies settings 
         */

        public static string COOKIE_LANG
        {
            get { return "qweb_lang_local"; }
        }


        #endregion

        #region Constructor

        public Local()
        {
        }

        #endregion
    }
}