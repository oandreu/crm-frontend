mainApp.directive('editableRow', [function(){
	return {
		scope: {},
		require: 'ngModel',
		restrict: 'A',
		link: function($scope, el, attrs, ngModel) {
			$scope.$on('$editableRow', function(nVal){
				console.log(nVal);
			});
		}
	};
}]);