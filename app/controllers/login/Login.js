'use strict';

mainApp.controller('Login', ['$scope', '$rootScope', 'fEffects', 'fLogin', 
                        function ($scope,   $rootScope,   fEffects,   fLogin) {

    $scope.formLogin = {
        "username": "",
        "password": ""
    };

	// function _error(actualUsername){
    function _error(credentials, attempt){ 

      
        if (credentials.username == "" || credentials.password == "") {
            $scope.auth_error = true;
            $scope.error_message = "Enter any username and password."
            return;
        }

        if (credentials.username != attempt.username 
            && credentials.password != attempt.password ) {
            $scope.auth_error = true;
            $scope.error_message = "Username or password are incorrect!";
            return;
        };


        // $scope.errorPassword = true;

        // if(actualUsername == $scope.formLogin.username){
        //     $scope.errorUsername = false;
        // }
        // else{
        //     $scope.errorUsername = true;
        // }

        // return fEffects.shake('#adminLogin');
    }

    $scope.login = function(){
        // $scope.errorUsername = false;
        // $scope.errorPassword = false;

        // if ($scope.formLogin.username.length == 0) { $scope.errorUsername = true; }
        // if ($scope.formLogin.password.length == 0) { $scope.errorPassword = true; }
        // if (angular.isUndefined($scope.formLogin)) { console.log("IMPOSIBLE");return _error(); }

		var credentials = {
			"username" : $scope.formLogin.username,
			"password" : $scope.formLogin.password
		}

        var attempt = fLogin.attempt(credentials);

        if(!attempt.authenticated){
            _error(credentials, attempt);
            return;
        }

 
        fLogin.username = attempt.username;

	}


	$scope.$on('$viewContentLoaded', function () {
	    $('body').addClass("login");
	});

	$scope.$on('$destroy', function () {
	    $('body').removeClass("login");
	});


}]);



















 //    $scope.fLogin = fLogin;                          

	// $scope.login = function() {
 //    //     if(!fLogin.UserName || !fLogin.Password || fLogin.UserName.length == 0 || fLogin.Password.length == 0) {
	// 	  // fEffects.shake('#adminLogin');
 //    //       return;
 //    //     }

 //    //     fLogin.validateLogin(fLogin.UserName ,fLogin.Password)
	// }

