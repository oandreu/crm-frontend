
/* Setup general page controller */
mainApp.controller('GeneralPageController', ['$rootScope', '$scope', function($rootScope, $scope) {
   	
    $scope.$on('$viewContentLoaded', function() {   
    	// initialize core components
    	Metronic.initAjax();
    	
    	// set default layout mode
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });

}]);
