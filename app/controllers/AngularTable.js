mainApp.controller('AngularTable', 
           [ '$rootScope', '$scope', '$http', '$filter', '$modal', 'fProfiles',
   function(  $rootScope ,  $scope ,  $http,   $filter,   $modal,   fProfiles){ 

  // items
  $scope.items           = []; // the items of the list
  $scope.filtered        = [];
  $scope.selectedIndex   = null; // current index selected in the table
  $scope.selectedItem    = null // current item selected in the table
  // http call for retrieve the list of items
  fProfiles.profiles().then(function(result){
    $scope.items = result.data.profiles;
    $scope.tableReady = true; // table is ready to go
    $rootScope.$broadcast("$updateTable");
  });
  $scope.isEmpty = function(){
    return $scope.filtered.length === 0;
  }

  // pagination
  $scope.currentPage     = 1;
  $scope.numPerPageRange = [5, 10, 20, 50];
  $scope.numPerPage      = $scope.numPerPageRange[0];

  // sorting 
  $scope.currentSelectedField = "username";
  $scope.reversedSort         = false;
  $scope.sort = function(sortValue){
    $scope.currentSelectedField = sortValue;
    $scope.reversedSort = !$scope.reversedSort;
  } 

  // reset all fields
  function _resetAllActions(){
    $scope.isAdding = false;
    $scope.newItem = null;
    $scope.selectedIndex = null;
    $scope.selectedItem = null;
  }

  $rootScope.$on("$updateTable", function(){
    // watching search field, currentPage selected in pagination and numPerPage value by the dropdown
    $scope.$watch('search + currentPage + numPerPage + currentSelectedField + reversedSort', function(){
      var begin         = ($scope.currentPage - 1) * $scope.numPerPage;
      var end           = begin + $scope.numPerPage;
      var filByField    = $filter('filter')($scope.items, $scope.search); // filtered by search field
      var filByOrder    = $filter('orderBy')(filByField, $scope.currentSelectedField, $scope.reversedSort); // order by column field
      var filByPage     = filByOrder.slice(begin, end); // filtered by pagination
      $scope.filtered   = filByPage; // current filtered items in the view per page
      $scope.totalItems = filByField.length; // current items length filtered
      _resetAllActions();
    });
  });

  // Edit mode on/off
  $scope.isEditable = function(index){
    return index === $scope.selectedIndex;
  }

  // set adding statement to true
  $scope.addNew = function(){
     $scope.isAdding = true;
  }

  // set adding statement to false and reset the newItem object
  $scope.removeAddNew = function(){
     $scope.isAdding = false;
     if (angular.isUndefined($scope.newItem)) return;
     $scope.newItem = null;
  }

  $scope.add = function(){
    if (angular.isUndefined($scope.newItem)) return;
    $scope.items.push({
       "username" : $scope.newItem.username,
       "fullname" : $scope.newItem.fullname,
       "points"   : $scope.newItem.points,
       "status"   : $scope.newItem.status,
       "notes"    : $scope.newItem.notes
    });
    $rootScope.$broadcast("$updateTable");
    toastr.success('Item added successfully!');
  }

  $scope.save = function(item, index){
    ////////////////////////////////////////
    // do something aync for save the list
    ////////////////////////////////////////
    $rootScope.$broadcast("$updateTable");
    toastr.success("Item saved!");
  }

  $scope.edit = function(index){
    $scope.selectedIndex = index;
    $scope.selectedItem  = angular.copy($scope.filtered[index]);
  }

  $scope.cancel = function(item){
    if (!angular.equals($scope.selectedItem, item)) {
      for(var property in item){
        if (property !== "$$hashKey"){
          item[property] = $scope.selectedItem[property];
        } 
      }
    }
    $rootScope.$broadcast("$updateTable");
  }

  $scope.openModal = function(item, index){
       var modalInstance = $modal.open({
          templateUrl : "app/views/modal/deleteFromTable.html",
          controller : "AngularTableModal",
          resolve : {
             result : function(){
                return{
                   items : $scope.items,
                   filtered : $scope.filtered,
                   index : index,
                   item : item
                }
             }
          }
       });
    }

  $scope.$on('$viewContentLoaded', function() {   
     // initialize core components
     Metronic.initAjax();
  });
   
}])

 
.controller('AngularTableModal', 
         [ '$scope', '$rootScope', '$modalInstance', 'result', 
   function($scope,   $rootScope,   $modalInstance,   result ){

      $scope.items    = result.items;
      $scope.filtered = result.filtered;

      $scope.delete = function(){
         var item = result.item;
         $scope.items.splice($scope.items.indexOf(item), 1)
         $scope.filtered.splice($scope.filtered.indexOf(item), 1)
         $rootScope.$broadcast("$updateTable");
         $modalInstance.dismiss('cancel');
         toastr.success(item.fullname + ' removed successfully!');
      }

      $scope.close = function(){
         $modalInstance.dismiss('cancel');
      }
   
}]);

