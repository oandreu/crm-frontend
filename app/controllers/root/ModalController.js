/* Controller for Layout Modals */
mainApp.controller('ModalController', ['$scope', '$modal', '$modalInstance',
                                   function($scope,   $modal,   $modalInstance) {

    $scope.closeModal = function () {
        $modalInstance.close()
    }           
                             
}]);