/* Setup Layout Part - Header */
mainApp.controller('HeaderController', ['$scope', '$modal', 'fLogin',
                                    function($scope,   $modal,   fLogin) {

    $scope.fLogin = fLogin;

    /* ng-click for Open change Password Modal. */
    $scope.openModal = function () {

        $modal.open({ 
            templateUrl: 'modalChangePassword.html',
            windowClass: 'change-password',
            controller: 'ModalController'
        });
    };

    /* ng-click for log Out */
    $scope.logOut = function() {
        fLogin.clear();
    }

    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
}]);