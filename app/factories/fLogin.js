mainApp.factory('fLogin', ['$http', '$state', 'fEffects', 
					   function($http,   $state,   fEffects) {
		
		var $this = {};
		
		$this.username      = "admin",
		$this.password      = "admin",
		$this.authenticated = false,
		$this.token         = null;	

	/**
	 * fake token generator
	 * @return {number}
	 */
	function _random_token(){
		var token_range_num = 1000000;
		return Math.round(Math.random() * token_range_num);
	}



	return {
		/**
		 * check if the user is logged in
		 * @return {Boolean}
		 */
		isLoggedIn : function(force){
			if (force) return true;
			return $this.authenticated;
		},
		/**
		 * check if there's a token saved
		 * @return {number}
		 */
		haveToken : function(){
			return $this.token ;
		},
		/**
		 * check if the token on http header is equal to the token saved here
		 * @param  {number} tokenToMatch
		 * @return {boolean}
		 */
		tokenMatch : function(tokenToMatch){
			return $this.token  === tokenToMatch;
		},
		/**
		 * this is the login function
		 * @param  {object} credentials
		 * @return {boolean}
		 */
		attempt : function(credentials){
			/**
			 * fake login
			 * @type {boolean}
			 */
			if ($this.username === credentials.username && $this.password === credentials.password) {
				$this.username = credentials.username;
				$this.authenticated = true;
				$this.token = _random_token();
				
				/**
				 * attaching the token to the headers
				 */
				$http.defaults.headers.common['auth-token'] = $this.token;
				
				/**
				 * redirect to the dashboard
				 */
				$state.go('dashboard');

				return $this;
			}
			
			return $this;
		},
		clear : function(){
			$this.authenticated = false,
			$this.token         = null;	

			$http.defaults.headers.common['auth-token'] = null;

			$state.go('login');

			return $this;
		}
	}
}]);

