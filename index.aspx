﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="_Index" %>
<%
    HttpCookie cookielang = Request.Cookies[ QWeb.QConfig.COOKIE_LANG ];
    string lang = (cookielang != null && !string.IsNullOrEmpty(cookielang.Value)) ? cookielang.Value : QWeb.QConfig.DEFAULT_LANG;
%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js" ng-app="mainApp"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js" ng-app="mainApp"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<% Response.Write(lang); %>"  ng-app="mainApp">
<!--<![endif]-->
<!-- BEGIN HEAD -->
    <head>
        <title ng-bind="'BetUniq | ' + $state.current.data.pageTitle"></title>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <base href="<% Response.Write(QWeb.QConfig.HOMEBASE); %>" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/global/plugins/bootstrap-toastr/toastr.min.css">
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN DYMANICLY LOADED CSS FILES(all plugin and page related styles must be loaded between GLOBAL and THEME css files ) -->
        <link id="ng_load_plugins_before"/>
        <!-- END DYMANICLY LOADED CSS FILES -->

        <!-- BEGIN THEME STYLES -->
        <link href="assets/global/css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/themes/bq_theme.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->

        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body ng-controller="AppController" class="page-header-fixed page-sidebar-closed-hide-logo page-quick-sidebar-over-content page-on-load" ng-class="{'page-container-bg-solid': settings.layout.pageBodySolid, 'page-sidebar-closed': settings.layout.pageSidebarClosed}">

        <div id="appMainView">
            <!-- BEGIN PAGE SPINNER -->
            <div ng-spinner-bar class="page-spinner-bar">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
            <!-- END PAGE SPINNER -->

            <!-- BEGIN HEADER -->
            <div ng-include="'app/views/root/header.html'" ng-controller="HeaderController" class="page-header navbar navbar-fixed-top">
            </div>
            <!-- END HEADER -->

            <div class="clearfix">
            </div>

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div ng-include="'app/views/root/sidebar.html'" ng-controller="SidebarController" class="page-sidebar-wrapper">          
                </div>
                <!-- END SIDEBAR -->

                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- BEGIN ACTUAL CONTENT -->
                        <div ui-view class="fade-in-up"></div> 
                        <!-- END ACTUAL CONTENT -->
                    </div>  
                </div>
                <!-- END CONTENT -->
                <!-- test -->
                <!-- BEGIN QUICK SIDEBAR -->
                <a href="javascript:;" class="page-quick-sidebar-toggler"><i class="icon-close"></i></a>
                <div ng-include="'app/views/root/quick-sidebar.html'" ng-controller="QuickSidebarController" class="page-quick-sidebar-wrapper"></div>
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->

            <!-- BEGIN FOOTER -->
            <div ng-include="'app/views/root/footer.html'" ng-controller="FooterController" class="page-footer">
            </div>
            <!-- END FOOTER -->
        </div>
         <!-- END APPMAINVIEW -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

        <!-- BEGIN CORE JQUERY PLUGINS -->
        <!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>

        <script src="assets/global/plugins/underscore/underscore.js" type="text/javascript"></script>
        <script src="assets/global/plugins/underscore.string/dist/underscore.string.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>

        <script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> 
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/media/js/jquery.datatables.js"></script>
        <!-- END CORE JQUERY PLUGINS -->

        <!-- BEGIN CORE ANGULARJS PLUGINS -->
        <script src="assets/global/plugins/angularjs/angular.min.js" type="text/javascript"></script>  
        <script src="assets/global/plugins/angularjs/angular-sanitize.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/angularjs/angular-touch.min.js" type="text/javascript"></script>    
        <script src="assets/global/plugins/angularjs/plugins/angular-ui-router.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/angularjs/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
    
        <script src="assets/global/plugins/angularjs/plugins/ui-bootstrap-tpls.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/angular-datatables/dist/angular-datatables.js"></script>
        <!-- END CORE ANGULARJS PLUGINS -->
    
        <!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
        <script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
        <script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
        <script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>  
        <!-- END APP LEVEL JQUERY SCRIPTS -->

        <!-- BEGIN APP LEVEL ANGULARJS SCRIPTS -->
        <script src="app/app.js" type="text/javascript"></script>
        <!-- END APP LEVEL ANGULARJS SCRIPTS -->

        <!-- FACTORIES -->
        <script src="app/factories/fEffects.js" type="text/javascript"></script>
        <script src="app/factories/fLogin.js" type="text/javascript"></script>
        <script src="app/factories/fTokenInterceptor.js" type="text/javascript"></script>
        <!-- END FACTORIES -->

        <!-- DIRECTIVES -->
        <script src="app/directives/a.js" type="text/javascript"></script>
        <script src="app/directives/SpinnerBar.js" type="text/javascript"></script>
        <!-- END DIRECTIVESS -->

        <!-- CONTROLLERS -->
        <script src="app/controllers/root/AppController.js" type="text/javascript"></script>
        <script src="app/controllers/root/HeaderController.js" type="text/javascript"></script>
        <script src="app/controllers/root/FooterController.js" type="text/javascript"></script>
        <script src="app/controllers/root/ModalController.js" type="text/javascript"></script>
        <script src="app/controllers/root/QuickSidebarController.js" type="text/javascript"></script>
        <script src="app/controllers/root/ThemePanelController.js" type="text/javascript"></script>
        <script src="app/controllers/root/SidebarController.js" type="text/javascript"></script>
        <!-- END DIRECTIVESS -->
        
        <!-- END JAVASCRIPTS -->

        <script type="text/javascript">
            /* Init Metronic's core jquery plugins and layout scripts */
            $(document).ready(function() {   
                Metronic.init(); // Run metronic theme
                Metronic.setAssetsPath('/assets/'); // Set the assets folder path           
            });
        </script>
    </body>
    <!-- END BODY -->
</html>